function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$(function() {
    $('#recover-submit').click(function() {

        var email = $("#email").val();
        if(validateEmail(email)){
            $("#err").hide();
            $("#recover-submit").html('Sending...');

            $("#recover-submit" ).prop( "disabled", true );
            $.ajax({
                url: '/sendmMail',
                data: {'email': "markaguirre26@gmail.com"},
                type: 'POST',
                success: function(response) {
                    console.log(response.status.msg);
                    $("#recover-submit").html('Reset Password');
                    $("#recover-submit" ).prop( "disabled", false );
                    alert("Please check your email!");
                    window.location.href = flask_util.url_for('login', {});
                    // var r = response.status.msg;

                }

            });
        }else {
            $("#err").show();
        }


    });
});