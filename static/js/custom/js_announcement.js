$(document).ready(function(){

    $(function () {
        $.ajax({
            type: "GET",
            url: "/announcement/fetch",
            dataType: "json",
            success: function(data){
                var table =   $('#records_table').DataTable({
                    "bProcessing": true,
                    "deferRender": true,
                    data: data,
                    columns:[
                        {title:"#"},
                        {title: "Subject"},
                        {title:"Date"},
                        {title:"isDeleted"},
                        {title:""}
                    ],"columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<a href='#' id='a_delete' class='glyphicon glyphicon-remove'></a><span><a href='#'  id='a_info' style='margin-left: 10px' class='glyphicon glyphicon-info-sign'></a></span>",

                    } ]

                });
                $('#records_table tbody').on('click', '#a_delete', function () {
                    var d = table.row( $(this).parents('tr') ).data();
                    if(confirm("Are you sure you want to remove "+ d[2]+"?")){

                        $.post("announcement/delete",{cn: d[0]});
                        table.row( $(this).parents('tr')).remove().draw();
                    }

                } );
                $('#records_table tbody').on('click', '#a_info_user', function () {
                    var d = table.row( $(this).parents('tr') ).data();
                    $.ajax({
                        type:"GET",
                        url:"/announcement/getbyid/"+d[0],
                        success: function(data){
                            $("#cmd_save").hide();
                            $("#txt_subject").val(data.records.subject);
                            $("#txt_description").val(data.records.description);
                            $('#myModal').modal('show');
                        }
                    });
                    $.ajax({
                        url: '/anmtisNew',
                        data: {"cn":d[0].toString()},
                        type: 'POST',
                        success: function(response) {
                            console.log(response);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                } );
            },
            error:function (xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }

        });
    });
});/**
 * Created by Asus on 6/2/2017.
 */
