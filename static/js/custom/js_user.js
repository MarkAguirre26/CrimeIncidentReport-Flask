$(document).ready(function(){
    $("#cmd_new").click(function () {
        $("#modal_title").text("Create Account");
        $("#div_pwd").show();
        // $("#div_since").hide();
        $("#txt_label_since").hide();

        $("#cmd_save_user").text("Save");
        $("input[name=txt_action]").val("New");
        $("input[name=txt_name]").val('');
        $("input[name=txt_email]").val('');
        $("input[name=txt_password]").val('');
        $("select[name=cbo_gender]").val('');
        $("select[name=cbo_type]").val('');
        $("input[name=txt_age]").val('');
        $("textarea[name=txt_address]").val('');
        $("input[name=txt_since]").hide();
        $('#userModal').modal('show');


    });

    $(function() {
        $('#userx').click(function() {
            $("#user_error").hide();
        });
    });
    $(function () {
        $( "input[name=txt_password]" ).change(function() {
            // alert( "Handler for .change() called." );
            var pwd =  $("input[name=txt_password]").val();
            if(pwd.length < 6){
                $("#pwd_validate").show();
            }else {
                $("#pwd_validate").hide();
            }
        });
    });
    $(function () {
        $( "input[name=txt_repeatpassword]" ).change(function() {
            // alert( "Handler for .change() called." );
            var pwd =  $("input[name=txt_password]").val();
            var repwd =  $("input[name=txt_repeatpassword]").val();
            if(pwd != repwd){
                $("#repwd_validate").show();
            }else {
                $("#repwd_validate").hide();

            }
        });
    });
    $(function() {
        $('#cmd_save_user').click(function() {
            $.ajax({
                url: '/ModifyUsaer',
                data: {'cn': $("#txt_cn").val(), 'action': $("#txt_action").val(), 'name': $("#txt_name").val(), 'email': $("#txt_email").val(), 'password':$("#txt_password").val(), 'repeatpassword': $("#txt_repeatpassword").val(), 'gender': $("#cbo_gender").val(), 'usertype': $("#cbo_type").val(), 'age': $("#txt_age").val(), 'address': $("#txt_address").val()},
                type: 'POST',
                success: function(response) {
                    console.log(response);
                    var r = response.status.status;
                    try {
                        if(r == "False"){
                            // alert(r);
                            $("#user_error").show();

                        }else{
                            $('#userModal').modal('hide');
                            window.location.href = flask_util.url_for('user', {});
                        }
                    }
                    catch(err) {
                        $("#user_error").show();

                    }
                }

            });
        });
    });

    $(function () {
        $.ajax({
            type: "GET",
            url: "/user/fetch",
            dataType: "json",
            success: function(data){
                var table =   $('#records_table').DataTable({
                    "bProcessing": true,
                    "deferRender": true,
                    data: data,
                    columns:[
                        {title:"#"},
                        {title: "Name"},
                        {title:"Gender"},
                        {title:"Age"},
                        {title:""}
                    ],"columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<a href='#' id='a_delete_user' class='glyphicon glyphicon-remove'></a><span><a href='#'  id='a_info_user' style='margin-left: 10px' class='glyphicon glyphicon-info-sign'></a></span>",

                    } ],
                     "order": [[ 3, "desc" ]]

                });
                $('#records_table tbody').on('click', '#a_delete_user', function () {
                    var d = table.row( $(this).parents('tr') ).data();
                    if(confirm("Are you sure you want to remove "+ d[2]+"?")){
                        table.row( $(this).parents('tr')).remove().draw();
                        $.ajax({
                            url: '/user/deleteuser',
                            data: {"cn":d[0].toString()},
                            type: 'POST',
                            success: function(response) {
                                //console.log(response);
                            },
                            error: function(error) {
                                console.log(error);
                            }
                        });
                    }

                } );
                $('#records_table tbody').on('click', '#a_info_user', function () {
                    var d = table.row( $(this).parents('tr') ).data();
                    $.ajax({
                        type:"GET",
                        url:"/user/getbyid/"+d[0],
                        success: function(data){
                            $("#modal_title").text("User Details");
                            $("#div_pwd").hide();
                              $("#txt_label_since").show();
                            $("input[name=txt_cn]").val(data.records.cn);
                            $("input[name=txt_action]").val("Update");
                            $("input[name=txt_name]").val(data.records.name);
                            $("input[name=txt_email]").val(data.records.email);
                            $("select[name=cbo_gender]").val(data.records.gender);
                            $("select[name=cbo_type]").val(data.records.usertype);
                            $("input[name=txt_age]").val(data.records.age);
                            $("textarea[name=txt_address]").val(data.records.address);
                            $("input[name=txt_since]").val(data.records.dateregister);
                            $("input[name=txt_since]").show();
                            $('#cmd_save_user').text("Update")
                            $('#userModal').modal('show');

                        }

                    });

                    $.ajax({
                        url: '/userisNew',
                        data: {"cn":d[0].toString()},
                        type: 'POST',
                        success: function(response) {
                            //console.log(response);
                        },
                        error: function(error) {
                            console.log(error);

                        }
                    });
                } );
            },
            error:function (xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }

        });
    });
});