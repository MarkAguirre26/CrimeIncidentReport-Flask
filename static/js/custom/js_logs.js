$(document).ready(function(){




    $(function () {
        $.ajax({
            type: "GET",
            url: "/logs/fetch",
            dataType: "json",
            success: function(data){
                console.log(data);
               $('#records_table').DataTable({
                    "bProcessing": true,
                    "deferRender": true,
                    data: data,
                    columns:[
                        {title:"#"},
                        {title: "Facebook ID"},
                        {title:"Name"},
                        {title:"Email"},
                        {title:"DateTime"},

                    ],
                   "order": [[ 3, "desc" ]]

                });

            },
            error:function (xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }

        });
    });
});